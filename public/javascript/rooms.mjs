import { createRoomCard, createRoomsPage } from './helpers/domHelper.mjs';
import { socket, username } from './game.mjs';

const roomsPage = createRoomsPage(username);
const roomsContainer = document.querySelector('#room-container');

const updateRooms = (rooms) => {
  const allRooms = rooms.map((room) => {
    const roomName = room[0];
    const usersCount = Object.keys(room[1]).length;
    return createRoomCard(roomName, usersCount);
  });
  roomsContainer.innerHTML = '';
  roomsContainer.append(...allRooms);
};

socket.on('UPDATE_ROOMS', updateRooms);

socket.on('NEW_ROOM_CREATED', (roomName) => {
  const roomCard = createRoomCard(roomName);
  roomsContainer.append(roomCard);
});

socket.on('JOIN_ROOM_DONE', (roomName) => {
  roomsPage.innerHTML = '';
});
